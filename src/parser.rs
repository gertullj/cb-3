use crate::*;
use lexer::C1Token::*;
pub struct C1Parser();

impl C1Parser {
    pub fn parse(source: &str) -> ParseResult {
        let mut lexer = C1Lexer::new(source);
        Self::parse_program(&mut lexer)
    }

    fn expect(lexemes: &mut C1Lexer, expected: C1Token) -> ParseResult {
        lexemes
            .current_token()
            .ok_or(format!(
                "Error at line {}: unexpected end of input.",
                lexemes.current_line_number().get_or_insert(0)
            ))
            .map(|token| {
                if token == expected {
                    Ok(())
                } else {
                    /*panic!(
                        "Error: wrong token {:?}, expected token {:?}.",
                        token, expected
                    );*/
                    ParseResult::Err(format!(
                        "Error at line {}: wrong token {:?}, expected {:?}.",
                        lexemes.current_line_number().get_or_insert(0),
                        token,
                        expected
                    ))
                }
            })?
    }

    fn expect_and_eat(lexemes: &mut C1Lexer, expected: C1Token) -> ParseResult {
        Self::expect(lexemes, expected)?;
        lexemes.eat();
        Ok(())
    }

    fn parse_program(lexemes: &mut C1Lexer) -> ParseResult {
        while lexemes.current_token().is_some() {
            Self::parse_function_definition(lexemes)?;
        }
        Ok(())
    }

    fn parse_function_definition(lexemes: &mut C1Lexer) -> ParseResult {
        Self::parse_type(lexemes)?;
        Self::expect_and_eat(lexemes, Identifier)?;
        Self::expect_and_eat(lexemes, LeftParenthesis)?;
        Self::expect_and_eat(lexemes, RightParenthesis)?;
        Self::expect_and_eat(lexemes, LeftBrace)?;
        Self::parse_statement_list(lexemes)?;
        Self::expect_and_eat(lexemes, RightBrace)
    }

    fn parse_function_call(lexemes: &mut C1Lexer) -> ParseResult {
        Self::expect_and_eat(lexemes, Identifier)?;
        Self::expect_and_eat(lexemes, LeftParenthesis)?;
        Self::expect_and_eat(lexemes, RightParenthesis)
    }

    fn parse_statement_list(lexemes: &mut C1Lexer) -> ParseResult {
        while lexemes.current_token() != Some(RightBrace) {
            Self::parse_block(lexemes)?;
        }
        Ok(())
    }

    fn parse_block(lexemes: &mut C1Lexer) -> ParseResult {
        if lexemes.current_token() == Some(LeftBrace) {
            lexemes.eat();
            Self::parse_statement_list(lexemes)?;
            Self::expect_and_eat(lexemes, RightBrace)
        } else {
            Self::parse_statement(lexemes)
        }
    }

    fn parse_statement(lexemes: &mut C1Lexer) -> ParseResult {
        match lexemes.current_token() {
            Some(token) => {
                match token {
                    KwIf => return Self::parse_if_statement(lexemes),
                    KwReturn => Self::parse_return_statement(lexemes)?,
                    KwPrintf => Self::parse_printf(lexemes)?,
                    Identifier => match lexemes.peek_token() {
                        Some(LeftParenthesis) => Self::parse_function_call(lexemes)?,
                        Some(Assign) => Self::parse_stat_assignment(lexemes)?,
                        Some(unexpected) => {
                            return Err(format!(
                                "Error at line {}: unexpected token {:?} whilst parsing statement.",
                                lexemes.peek_line_number().get_or_insert(0),
                                unexpected
                            ))
                        }
                        None => {
                            return Err(format!(
                            "Error at line {}: unexpected end of input whilst parsing statement.",
                            lexemes.peek_line_number().get_or_insert(0)
                        ))
                        }
                    },

                    _ => {
                        return Err(format!(
                            "Error at line {}: unexpected token {:?} whilst parsing statement.",
                            lexemes.peek_line_number().get_or_insert(0),
                            token
                        ))
                    }
                }
                Self::expect_and_eat(lexemes, Semicolon)
            }
            None => Err(format!(
                "Error at line {}: unexpected end of input whilst parsing statement.",
                lexemes.peek_line_number().get_or_insert(0)
            )),
        }
    }

    fn parse_if_statement(lexemes: &mut C1Lexer) -> ParseResult {
        Self::expect_and_eat(lexemes, KwIf)?;
        Self::expect_and_eat(lexemes, LeftParenthesis)?;
        Self::parse_assignment(lexemes)?;
        Self::expect_and_eat(lexemes, RightParenthesis)?;
        Self::parse_block(lexemes)
    }

    fn parse_return_statement(lexemes: &mut C1Lexer) -> ParseResult {
        Self::expect_and_eat(lexemes, KwReturn)?;
        if lexemes.current_token() != Some(Semicolon) {
            Self::parse_assignment(lexemes)
        } else {
            Ok(())
        }
    }

    fn parse_printf(lexemes: &mut C1Lexer) -> ParseResult {
        Self::expect_and_eat(lexemes, KwPrintf)?;
        Self::expect_and_eat(lexemes, LeftParenthesis)?;
        Self::parse_assignment(lexemes)?;
        Self::expect_and_eat(lexemes, RightParenthesis)
    }

    fn parse_type(lexemes: &mut C1Lexer) -> ParseResult {
        // lexemes.eat();
        match lexemes.current_token() {
            Some(token) => match token {
                KwBoolean | KwFloat | KwInt | KwVoid => {
                    lexemes.eat();
                    Ok(())
                }
                _ => Err(format!(
                    "Error at line {}: unexpected token {:?} whilst parsing type.",
                    lexemes.current_line_number().get_or_insert(0),
                    token
                )),
            },
            None => Err(format!(
                "Error at line {}: Unexpected end of input whilst parsing type.",
                lexemes.current_line_number().get_or_insert(0),
            )),
        }
    }

    fn parse_stat_assignment(lexemes: &mut C1Lexer) -> ParseResult {
        Self::expect_and_eat(lexemes, Identifier)?;
        Self::expect_and_eat(lexemes, Assign)?;
        Self::parse_assignment(lexemes)
    }

    fn parse_assignment(lexemes: &mut C1Lexer) -> ParseResult {
        match (lexemes.current_token(), lexemes.peek_token()) {
            (Some(Identifier), Some(Assign)) => {
                lexemes.eat();
                lexemes.eat();
                // Self::expect_and_eat(lexemes, Assign)?;
                Self::parse_assignment(lexemes)
            }
            (Some(_), _) => Self::parse_expr(lexemes),
            (None, _) => Err(format!(
                "Error at line {}: unexpected end of input whilst parsing assignment.",
                lexemes.current_line_number().get_or_insert(0)
            )),
        }
    }

    fn parse_expr(lexemes: &mut C1Lexer) -> ParseResult {
        Self::parse_simple_expr(lexemes)?;
        match lexemes.current_token() {
            Some(Equal) | Some(NotEqual) | Some(LessEqual) | Some(GreaterEqual) | Some(Less)
            | Some(Greater) => {
                lexemes.eat();
                Self::parse_simple_expr(lexemes)
            }
            Some(_) | None => Ok(()),
        }
    }

    fn parse_simple_expr(lexemes: &mut C1Lexer) -> ParseResult {
        if lexemes.current_token() == Some(Minus) {
            lexemes.eat();
        }
        Self::parse_term(lexemes)?;
        loop {
            match lexemes.current_token() {
                Some(Plus) | Some(Minus) | Some(Or) => {
                    lexemes.eat();
                    Self::parse_term(lexemes)?;
                }
                Some(_) | None => break,
            }
        }

        Ok(())
    }

    fn parse_term(lexemes: &mut C1Lexer) -> ParseResult {
        Self::parse_factor(lexemes)?;
        loop {
            match lexemes.current_token() {
                Some(Asterisk) | Some(Slash) | Some(And) => {
                    lexemes.eat();
                    Self::parse_factor(lexemes)?;
                }
                Some(_) | None => break,
            }
        }

        Ok(())
    }

    fn parse_factor(lexemes: &mut C1Lexer) -> ParseResult {
        match lexemes.current_token() {
            Some(ConstInt) | Some(ConstFloat) | Some(ConstBoolean) => {
                lexemes.eat();
                Ok(())
            }
            Some(Identifier) => match lexemes.peek_token() {
                Some(LeftParenthesis) => Self::parse_function_call(lexemes),
                Some(_) | None => {
                    lexemes.eat();
                    Ok(())
                }
            },
            Some(LeftParenthesis) => {
                lexemes.eat();
                Self::parse_assignment(lexemes)?;
                Self::expect_and_eat(lexemes, RightParenthesis)
            }
            Some(unexpected) => {
                // panic!("");
                return Err(format!(
                    "Error at line {}: unexpected token {:?} whilst parsing factor.",
                    lexemes.current_line_number().get_or_insert(0),
                    unexpected
                ));
            }
            None => {
                return Err(format!(
                    "Error at line {}: unexpected end of input whilst parsing factor.",
                    lexemes.current_line_number().get_or_insert(0)
                ))
            }
        }
    }
}
